package it.emmea90.androtracks;

/**
 * Track Modes
 * @author Alessio
 *
 */
public enum TrackMode {
	PEDESTRIANS, DRIVE, BIKE;

	/**
	 * Get the route mode
	 * @return routemode
	 */
	public String getRouteMode() {
		if(this == TrackMode.DRIVE) {
			return "driving";
		} else if(this == TrackMode.PEDESTRIANS) {
			return "walking";
		} else if(this == TrackMode.BIKE) {
			return "bicycling ";
		}
		return "driving";
	}

	public static TrackMode getMode(String routemode) {
		if(routemode == "driving") {
			return TrackMode.DRIVE;
		} else if(routemode == "walking") {
			return TrackMode.PEDESTRIANS;
		} else if(routemode == "bicycling") {
			return TrackMode.BIKE;
		}
		return TrackMode.DRIVE;	
	}
}
