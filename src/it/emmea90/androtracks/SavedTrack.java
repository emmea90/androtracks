package it.emmea90.androtracks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;

/**
 * Class to implement a saved track object
 * @author Alessio Massetti
 *
 */
public class SavedTrack implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6016710147877229633L;
	private List<Double> latitudes;
	private List<Double> longitudes;
	private List<TrackMode> routemodes;
	
	/**
	 * Create a new Savedtrack. A SavedTrack is a list of markers points, saved as lists of all the latitudes and all the longitudes
	 */
	public SavedTrack() {
		latitudes = new ArrayList<Double>();
		longitudes = new ArrayList<Double>();
		routemodes = new ArrayList<TrackMode>();
	}
	
	/**
	 * Push a new point in the track
	 * @param pos LatLng of the track
	 * @param trackMode 
	 */
	public void push(LatLng pos, TrackMode trackMode) {
		latitudes.add(pos.latitude);
		longitudes.add(pos.longitude);
		routemodes.add(trackMode);
	}

	/**
	 * Pop a new point from the track
	 * @return LatLng point
	 */
	public LatLng popPosition() {
		LatLng position = new LatLng(latitudes.get(0), longitudes.get(0)); 
		latitudes.remove(0);
		longitudes.remove(0);
		return position;
	}
	
	/** 
	 * Pop a new trackmode from the track
	 * @return String trackmode
	 */
	public TrackMode popTrackMode() {
		try {
			TrackMode trackMode = routemodes.get(0);
			routemodes.remove(0);
			return trackMode;
		} catch (NullPointerException e) {
			return TrackMode.DRIVE;
		}
	}
	
	/**
	 * Returns points in the savedtrack
	 * @return points
	 */
	public int size() {
		return latitudes.size();
	}
}
