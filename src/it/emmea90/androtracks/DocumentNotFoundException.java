package it.emmea90.androtracks;

/**
 * Exception thrown when document can't be generated or is not found
 * @author Alessio
 *
 */
public class DocumentNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6750836314095563206L;

}
