package it.emmea90.androtracks;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;

/**
 * Class to Manage a Route Section
 * @author Alessio Massetti
 *
 */
public class RouteSection implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9037590723725224241L;
	private List<LatLng> points;
	private TrackMode routemode;
	
	/**
	 * Create a new section given the points
	 * @param sectionpoints
	 * @param trackmode 
	 */
	public RouteSection (ArrayList<LatLng> sectionpoints, TrackMode trackmode) {
		this.setPoints(sectionpoints);
		this.setRoutemode(trackmode);
	}

	/**
	 * Returns all points of the sections
	 * @return points
	 */
	public List<LatLng> getPoints() {
		return points;
	}

	/**
	 * Set the points of the section
	 */
	public void setPoints(List<LatLng> points) {
		this.points = points;
	}
	
	/**
	 * Get the routemode for this section
	 * @return routemode
	 */
	public TrackMode getRoutemode() {
		return routemode;
	}

	/**
	 * Sets the routemode for this section
	 * @param trackmode
	 * @param trackmode
	 */
	public void setRoutemode(TrackMode trackmode) {
		this.routemode = trackmode;
	}
	
	/** 
	 * Return first point of the section
	 */
	public LatLng getFirst() {
		return points.get(0);
	}
	
	/**
	 * Return last point of the section
	 */
	public LatLng getLast() {
		return points.get(points.size()-1);
	}

}
