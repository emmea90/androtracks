package it.emmea90.androtracks;

import java.io.File;
import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

/**
 * Activity to Load Tracks
 * @author Alessio
 *
 */
public class LoadTrackActivity extends ActionBarActivity  {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load_track);
		ListView listview = (ListView) findViewById(R.id.filelist);
		String root = Environment.getExternalStorageDirectory().toString();
		File dir = new File(root + "/tracks");		
		File[] filelist = dir.listFiles();
		String[] theNamesOfFiles = new String[filelist.length];
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < theNamesOfFiles.length; i++) {
		   theNamesOfFiles[i] = filelist[i].getName();
		   list.add(filelist[i].getName());
		}
	    MyCustomAdapter adapter = new MyCustomAdapter(list, this);   
	    listview.setAdapter(adapter);

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.load_track, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
}
