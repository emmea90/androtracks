package it.emmea90.androtracks;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;

/**
 * Class to implement a track
 * @author Alessio Massetti
 *
 */
public class Track implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8631575591246405603L;
	private List<RouteSection> calcroute;
	private List<RoutePoint> calcroutealt;
	private List<Marker> markers;
	private List<Polyline> polylines;
	
	/**
	 * Create a new Track
	 * @param mMap
	 */
	public Track (GoogleMap mMap) {
		calcroute = new ArrayList<RouteSection>();
		calcroutealt = new ArrayList<RoutePoint>();
		markers = new ArrayList<Marker>();
		polylines = new ArrayList<Polyline>();
	}

	/**
	 * Get all the track markers
	 * @return a List of the markers
	 */
	public List<Marker> getMarkers() {
		return markers;
	}
	
	/**
	 * Get the route of the track
	 * @return calcroute
	 */
	public List<RouteSection> getCalcroute() {
		return calcroute;
	}

	/**
	 * Get the route profiled of the track
	 * @return calcroutealt
	 */
	public List<RoutePoint> getCalcroutealt() {
		return calcroutealt;
	}

	/**
	 * Get the polylines vector of the track
	 * @return polylines
	 */
	public List<Polyline> getPolylines() {
		return polylines;
	}
	
}
