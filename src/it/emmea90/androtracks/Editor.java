package it.emmea90.androtracks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Activity for map editor and Track Viewer
 * @author Alessio Massetti
 *
 */
public class Editor extends ActionBarActivity implements OnMapClickListener, OnMapLongClickListener, OnMarkerDragListener, OnMarkerClickListener, GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

	private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 0;
	private LocationClient mLocationClient;
	private Location mCurrentLocation;
	private LatLng mPosition;
	private GoogleMap mMap;
	private Track track;
	private boolean lockcamera;
	private RouteManager routemanager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        Toast.makeText(this, "Started", Toast.LENGTH_SHORT).show();
		setContentView(R.layout.activity_editor);
		mLocationClient = new LocationClient(this, this, this);
		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		mMap.setOnMapClickListener(this);
		mMap.setOnMapLongClickListener(this);
		mMap.setOnMarkerClickListener(this);
		mMap.setOnMarkerDragListener(this);
		track = new Track(mMap);
		Bundle extras = getIntent().getExtras();
		routemanager = new RouteManager(track, mMap, this);
		lockcamera = false;
		if (extras != null) {
		    String value = extras.getString("TrackToLoad");
		    SavedTrack savedtrack = loadTrack(value);
		    processLoadedTrack(savedtrack);
		}
	}

	/**
	 * Processed a saved track to load it in the activity
	 * @param savedtrack
	 */
	private void processLoadedTrack(SavedTrack savedtrack) {
		int size = savedtrack.size();
		for(int i = 0; i < size; i++) {
			LatLng position = savedtrack.popPosition();
			TrackMode trackMode = savedtrack.popTrackMode();
			if(trackMode != null) {
				routemanager.setTrackmode(trackMode);
			}
			routemanager.addMarker(position);
			if(i==0) {
				  lockcamera = true;
				  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 12));	
			}
		}
	}

	/**
	 * Load a saved track from a string
	 * @param value
	 * @return SavedTrack
	 */
	private SavedTrack loadTrack(String value) {
		String root = Environment.getExternalStorageDirectory().toString();
		File myDir = new File(root + "/tracks/" + value);  
		SavedTrack savedtrack = null;
		
		try {
			InputStream fin = new FileInputStream (myDir);
			ObjectInputStream in = new ObjectInputStream(fin);
			savedtrack = (SavedTrack) in.readObject();
			in.close();
			fin.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return savedtrack;
	}

	/**
	 * Initialize activity (called only on start)
	 */
	private void initialize() {
		mMap.setMyLocationEnabled(true);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);

	}	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.editor, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.mapnormal:
	    		mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		        Toast.makeText(this, "Normal Map on", Toast.LENGTH_SHORT).show();
	            return true;
	        case R.id.maphybrid:
	    		mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
		        Toast.makeText(this, "Hybrid Map on", Toast.LENGTH_SHORT).show();
	            return true;
	        case R.id.mapsatellite:
	    		mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		        Toast.makeText(this, "Satellite Map on", Toast.LENGTH_SHORT).show();
	            return true;
	        case R.id.mapterrain:
	    		mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
		        Toast.makeText(this, "Terrain Map on", Toast.LENGTH_SHORT).show();
	            return true;
	        case R.id.save:
		        Toast.makeText(this, "Saving", Toast.LENGTH_SHORT).show();
		        requestSavename();
	            return true;	 
	        case R.id.search:
		        Toast.makeText(this, "Searching", Toast.LENGTH_SHORT).show();
		        searchInMap();
	            return true;		      
	        case R.id.pedestrians:
	        	routemanager.setTrackmode(TrackMode.PEDESTRIANS);
	            return true;		                 
	        case R.id.drive:
	        	routemanager.setTrackmode(TrackMode.DRIVE);
	            return true;		 
	        case R.id.bike:
	        	routemanager.setTrackmode(TrackMode.BIKE);
	            return true;		            
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	/**
	 * Search in map the query given
	 */
	private void searchInMap() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Search");
		alert.setMessage("Insert Name or Coordinate");		
		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			/**
			 * Launch the search in map clicking on the alert button
			 * @param dialog the Dialog Interface
			 * @param whichButton the button clicked
			 */
			public void onClick(DialogInterface dialog, int whichButton) {
			  String value = input.getText().toString();
			  Geocoder geocoder = new Geocoder(Editor.this);
			  List<android.location.Address> addresses;
			try {
				addresses = geocoder.getFromLocationName(value,5);
				  if (addresses.size() > 0) {
					  LatLng p = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
					  float zoom = mMap.getCameraPosition().zoom;
					  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(p, zoom));	        
				  } else {
	                  AlertDialog.Builder adb = new AlertDialog.Builder(Editor.this);
	                  adb.setTitle("Not Found");
	                  adb.setMessage("Please Retry");
	                  adb.setPositiveButton("Close",null);
	                  adb.show();				 
				  }				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		});
		
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  /**
		   * Actions performed on click on negative button
		   */
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});
		
		alert.show();
	}

	/**
	 * Requests a name to save the track
	 */
	private void requestSavename() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Save");
		alert.setMessage("Insert Name");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		
		/**
		 * Action performed on click on positive button
		 */
		public void onClick(DialogInterface dialog, int whichButton) {
		  String value = input.getText().toString();
		  saveTrack(value);
		}

		/**
		 * Save the track
		 * @param value trackname
		 */
		private void saveTrack(String value) {
			FileOutputStream fos;
			
			String root = Environment.getExternalStorageDirectory().toString();
			File myDir = new File(root + "/tracks");    
			myDir.mkdirs();  
			File file = new File (myDir, value);

			try {
				fos = new FileOutputStream(file);
				ObjectOutputStream os = new ObjectOutputStream(fos);
				SavedTrack savedtrack = exportForSaving(track);
				os.writeObject(savedtrack);
				os.flush();
				os.close();
				fos.close();				
			} catch (Exception e) {
			    Log.e("FILE EXCEPTION", "IO Error", e);
			}
		}

		/**
		 * Export a track for saving
		 * @param track
		 * @return a SavedTrack
		 */
		private SavedTrack exportForSaving(Track track) {
			Iterator<RouteSection> itr = track.getCalcroute().iterator();
			SavedTrack savedtrack = new SavedTrack();
			boolean first = true;
		      while(itr.hasNext()) {
		    	  RouteSection element = itr.next();
		    	  if(first == true) {
			    	  savedtrack.push(element.getFirst(), TrackMode.DRIVE);
			    	  first = false;
		    	  }
		    	  savedtrack.push(element.getLast(), element.getRoutemode());
		       }
			return savedtrack;
		}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		alert.show();
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            // showErrorDialog(connectionResult.getErrorCode());
        }
	}

    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
        mLocationClient.connect();
		initialize();
    }
    
    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();
        super.onStop();
    }    
	
	@Override
	public void onConnected(Bundle arg0) {
        // Display the connection status
        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
        // Load Position and Center Map
		mCurrentLocation = mLocationClient.getLastLocation();
		try {
			mPosition = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
			if(!lockcamera) {
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mPosition, 12));	  	
			}  			
		} catch (NullPointerException e) {	
		}    
	}

	@Override
	public void onDisconnected() {
        Toast.makeText(this, "Disconnected. Please re-connect.",
        Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onMapClick(LatLng position) {
		routemanager.addMarker(position);	
	}

	@Override
	public void onMapLongClick(LatLng position) {
	}
	

	@Override
	public void onMarkerDrag(Marker marker) {
	}

	@Override
	public void onMarkerDragEnd(Marker marker) {
        Toast.makeText(this, "Updating Point", Toast.LENGTH_SHORT).show();
		routemanager.updateRoute(marker);		
	}

	@Override
	public void onMarkerDragStart(Marker marker) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
        Toast.makeText(this, "Deleting Point", Toast.LENGTH_SHORT).show();
		int index = track.getMarkers().indexOf(marker);
		if(index != -1) {
			if(index != 0) {
				routemanager.setCurrentPointer(routemanager.getCurrentPointer()-1);
				track.getMarkers().get(index).setVisible(false);
				track.getMarkers().remove(index);		
				if(index == routemanager.getCurrentPointer()) {	
					if (index != 1) {
						track.getMarkers().get(index-1).setIcon(BitmapDescriptorFactory.fromAsset("arrivo.gif"));
					}
					try {
						track.getPolylines().get(index-1).setVisible(false);
						track.getPolylines().remove(index-1);
						track.getCalcroute().remove(index-1);
					} catch (IndexOutOfBoundsException e) {
						
					}
				} else {
					track.getPolylines().get(index).setVisible(false);
					track.getPolylines().remove(index);
					track.getCalcroute().remove(index);		
				}
				Marker nextMarker = track.getMarkers().get(index-1);	
				routemanager.updateRoute(nextMarker);					
			}			
		} else {
			// deleteMarkerObject(marker);
		}
		return true;
	}

	/**
	 * Handle the direction result
	 * @param directionPoints
	 * @param routemode
	 * @param sectionnumber
	 */
	public void handleGetDirectionsResult(ArrayList<LatLng> directionPoints, String routemode, int sectionnumber)
	{
		if(directionPoints.get(0) != null) {
			LatLng startlocation = directionPoints.get(0);
			LatLng endlocation = directionPoints.get(directionPoints.size()-1);
			Marker startmarker = track.getMarkers().get(sectionnumber-1);
			Marker endmarker = track.getMarkers().get(sectionnumber);
			startmarker.setPosition(startlocation);
			endmarker.setPosition(endlocation);
			if(routemode == "add") {
				routemanager.addRouteCallback(directionPoints);
			} else if (routemode == "edit") {		
				routemanager.editRouteCallback(directionPoints, sectionnumber-1);
			}			
		} else {
	        Toast.makeText(this, "Google API direction error, please retry", Toast.LENGTH_SHORT).show();			
		}

	}
	
}
