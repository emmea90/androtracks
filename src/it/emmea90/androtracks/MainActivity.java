package it.emmea90.androtracks;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

/**
 * Main Activity of the Android App
 * @author Alessio Massetti
 *
 */
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		return super.onOptionsItemSelected(item);
	}

	/** Called when the user clicks the Send button */
	public void openEditor(View view) {
	    Intent intent = new Intent(this, Editor.class);
	    startActivity(intent);
	}
	
	/** Called when the user clicks the Loader button */
	public void openLoader(View view) {
	    Intent intent = new Intent(this, LoadTrackActivity.class);
	    startActivity(intent);
	}	
}
