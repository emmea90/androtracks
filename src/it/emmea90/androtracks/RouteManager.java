package it.emmea90.androtracks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.graphics.Color;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Class to manage the Route
 * @author Alessio
 *
 */
public class RouteManager {

	private Track track;
	private GoogleMap mMap;
	private int currentPointer;
	private Editor trackeditor;
	private TrackMode trackmode;
	
	/**
	 * Get the number of sections in the route
	 * @return CurrentPointer
	 */
	public int getCurrentPointer() {
		return currentPointer;
	}

	/**
	 * Set the number of sections in the route
	 * @param currentPointer
	 */
	public void setCurrentPointer(int currentPointer) {
		this.currentPointer = currentPointer;
	}

	/**
	 * Create a new RouteManager
	 * @param track track that Routemanager has to manage
	 * @param mMap map to display the track
	 * @param trackeditor editor activity to display track
	 */
	public RouteManager (Track track, GoogleMap mMap, Editor trackeditor) {
		this.track = track;
		this.mMap = mMap;
		this.trackeditor = trackeditor;
		this.setTrackmode(TrackMode.DRIVE);
		currentPointer = 0;
	}
	
	/**
	 * Find the marker near the position clicked
	 * @param position
	 * @return marker
	 */
	public Marker findNearestMarker(LatLng position) {
		double distance = 9999;
		Marker marker = null;
		Iterator<Marker> itr = track.getMarkers().iterator();
		while(itr.hasNext()) {
		    Marker current = (Marker) itr.next();
		    double currentdistance = distancePoints(current.getPosition(), position);
		    if(currentdistance < distance) {
		    	distance = currentdistance;
		    	marker = current;
		    }
		}		
		return marker;
	}	
	
	/**
	 * Returns the air distance between two points given
	 * @param start
	 * @param end
	 * @return distance between start and end
	 */
	public double distancePoints(LatLng start, LatLng end) {
		double dtor = Math.PI/180;
		double r = 6378.14;
		double rlat1 = start.latitude * dtor;
		double rlon1 = start.longitude * dtor;
		double rlat2 = end.latitude * dtor;
		double rlon2 = end.longitude * dtor;
		
		double dlon = rlon1 - rlon2;
		double dlat = rlat1 - rlat2;
		
		double a = Math.sin(dlat/2) * Math.sin(dlat/2) +
				Math.sin(dlon/2) * Math.sin(dlon/2) * Math.cos(rlat1) * Math.cos(rlat2); 
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double d = r * c;	 
		return d;			
	}

	/**
	 * Add Marker at given position
	 * @param position
	 */
	public void addMarker(LatLng position) {
		if(currentPointer==0) {
			placeMarker(position, BitmapDescriptorFactory.fromAsset("partenza.gif"));
		} else {
			placeMarker(position, BitmapDescriptorFactory.fromAsset("arrivo.gif"));	
			if(currentPointer>2) {
				// Marker currentmarker = track.getMarkers().get(currentPointer-2);
				// currentmarker.setIcon(BitmapDescriptorFactory.fromAsset("TrackPointLogo.gif"));
			}
		}		
	}

	/**
	 * Place a Marker 
	 * @param location
	 * @param icon
	 */
	public void placeMarker(LatLng location, BitmapDescriptor icon) {
		Marker marker = insertMarker(location, icon);
		track.getMarkers().add(marker);
		if (currentPointer > 0) {
			addRouteSection(currentPointer);
		}
		currentPointer = currentPointer + 1;	
	}

	/**
	 * Add a route section at point given
	 * @param sectionnumber
	 */
	public void addRouteSection(int sectionnumber) {
		computeRouteSection(sectionnumber, "add");
	}
	
	/** 
	 * Edit a route section at point given
	 * @param sectionnumber
	 */
	public void editRouteSection (int sectionnumber) {
		computeRouteSection (sectionnumber, "edit");
	}
	
	/** 
	 * Insert a marker at point given
	 * @param location point to insert
	 * @param icon of the Marker
	 * @return the Marker
	 */
	public Marker insertMarker(LatLng location, BitmapDescriptor icon) {
		MarkerOptions markerOptions = new MarkerOptions(); 
		markerOptions.position(location);
		// markerOptions.icon(icon);
		markerOptions.draggable(true);
		Marker marker = mMap.addMarker(markerOptions);
		/* google.maps.event.addListener(marker, 'dragend', function(event) {updateRoute(marker)} );	
		google.maps.event.addListener(marker, 'rightclick', function(event) {deleteMarker(marker)} );	
		google.maps.event.addListener(marker, 'click', function(event) {transformMarker(marker)} );			*/
		return marker;
	}
	
	/**
	 * Compute a route section
	 * @param sectionnumber the section to computde
	 * @param routemode the mode to track
	 */
	private void computeRouteSection(int sectionnumber, String routemode) {
		LatLng start = track.getMarkers().get(sectionnumber-1).getPosition();
		LatLng end = track.getMarkers().get(sectionnumber).getPosition();		
		String routetrackmode = trackmode.getRouteMode();
		findDirections (start.latitude, start.longitude, end.latitude, end.longitude, routetrackmode, routemode, sectionnumber);
	}	
	
	@SuppressWarnings("unchecked")
	/**
	 * Find route directions
	 * @param fromPositionDoubleLat latitude of start position 
	 * @param fromPositionDoubleLong longitude of start position
	 * @param toPositionDoubleLat latitude of end position
	 * @param toPositionDoubleLong longitude of end position
	 * @param mode mode
	 * @param routemode route mode
	 * @param sectionnumber number of section to find
	 */
	private void findDirections(double fromPositionDoubleLat, double fromPositionDoubleLong, double toPositionDoubleLat, double toPositionDoubleLong, String mode, String routemode, int sectionnumber)
	{
	    Map<String, String> map = new HashMap<String, String>();
	    map.put(GetDirectionsAsyncTask.USER_CURRENT_LAT, String.valueOf(fromPositionDoubleLat));
	    map.put(GetDirectionsAsyncTask.USER_CURRENT_LONG, String.valueOf(fromPositionDoubleLong));
	    map.put(GetDirectionsAsyncTask.DESTINATION_LAT, String.valueOf(toPositionDoubleLat));
	    map.put(GetDirectionsAsyncTask.DESTINATION_LONG, String.valueOf(toPositionDoubleLong));
	    map.put(GetDirectionsAsyncTask.DIRECTIONS_MODE, mode);
	 
	    GetDirectionsAsyncTask asyncTask = new GetDirectionsAsyncTask(trackeditor, routemode, sectionnumber);
	    asyncTask.execute(map);
	}

	/**
	 * Callback in route edit
	 * @param directionPoints points found
	 * @param sectionnumber section number
	 */
	protected void editRouteCallback(ArrayList<LatLng> directionPoints, int sectionnumber) {
		track.getCalcroute().get(sectionnumber).setPoints(directionPoints);
		track.getPolylines().get(sectionnumber).setPoints(directionPoints);
		// updateRouteInfos();
	}
	
	/**
	 * Callback in route add
	 * @param directionPoints points found
	 * @param routemode 
	 * @param sectionnumber section number
	 */
	protected void addRouteCallback(ArrayList<LatLng> directionPoints) {
		track.getCalcroute().add(new RouteSection(directionPoints, trackmode));	
		drawRoute(directionPoints);	
		// updateRouteInfos();
	}

	/**
	 * Draw route on the map
	 * @param directionPoints the route
	 * @param routemode 
	 */
	private void drawRoute(ArrayList<LatLng> directionPoints) {
		Polyline poly = insertPoly(directionPoints, track.getPolylines().size());
		track.getPolylines().add(poly);	
	}
	
	/**
	 * Update the route near the marker given
	 * @param marker the marker given
	 */
	protected void updateRoute(Marker marker) {
		int order = track.getMarkers().indexOf(marker);
		if(order!=0){
			editRouteSection(order);		
		}
		if(order!=currentPointer-1){
			editRouteSection(order+1);	
		}		
		updateMarkers(marker);					
	}

	/** 
	 * Update route marker given
	 * @param marker the marker given
	 */
	private void updateMarkers(Marker marker) {
		marker.setPosition(marker.getPosition());
	}
	
	/**
	 * Insert a polyline in the map
	 * @param directionPoints points of polyline
	 * @param size size of polyline
	 * @param routemode 
	 * @return
	 */
	private Polyline insertPoly(ArrayList<LatLng> directionPoints, int size) {
		/* if(trasparency) {
			opacity = .9;
		} else {
			opacity = .3;
		} */
		Polyline poly;
		PolylineOptions rectLine = new PolylineOptions().width(3).color(getPolyColor());
	    for(int i = 0; i < directionPoints.size(); i++)
	    {
	        rectLine.add(directionPoints.get(i));
	    }
	    poly = mMap.addPolyline(rectLine);
		return poly;
	}

	private int getPolyColor() {
		if(trackmode == TrackMode.DRIVE) {
			return Color.RED;
		} else if(trackmode == TrackMode.PEDESTRIANS) {
			return Color.BLUE;
		} else if(trackmode == TrackMode.BIKE) {
			return Color.YELLOW;
		}
		return Color.RED;	
	}
	
	/**
	 * Get the track Mode
	 * @return TrackMode
	 */
	public TrackMode getTrackmode() {
		return trackmode;
	}

	/**
	 * Set the track Mode
	 * @param trackmode
	 */
	public void setTrackmode(TrackMode trackmode) {
		this.trackmode = trackmode;
	}
}
