package it.emmea90.androtracks;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MyCustomAdapter extends BaseAdapter implements ListAdapter { 
	private ArrayList<String> list = new ArrayList<String>(); 
	private Context context; 
	
	
	
	public MyCustomAdapter(ArrayList<String> list, Context context) { 
	    this.list = list; 
	    this.context = context; 
	} 
	
	@Override
	public int getCount() { 
	    return list.size(); 
	} 
	
	@Override
	public Object getItem(int pos) { 
	    return list.get(pos); 
	} 
	
	@Override
	public long getItemId(int pos) { 
	    return 0;
	    //just return 0 if your list items do not have an Id variable.
	} 
	
	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
	    View view = convertView;
	    if (view == null) {
	        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
	        view = inflater.inflate(R.layout.listview_layout, null);
	    } 
	
	    //Handle TextView and display string from your list
	    TextView listItemText = (TextView)view.findViewById(R.id.filetextlist); 
	    listItemText.setText(list.get(position)); 
	
	    //Handle buttons and add onClickListeners
	    Button loadBtn = (Button)view.findViewById(R.id.load_btn);
	    Button deleteBtn = (Button)view.findViewById(R.id.delete_btn);	    
	    
	    loadBtn.setOnClickListener(new View.OnClickListener() {	
			@Override
			public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Editor.class);
                String message = (String) list.get(position);
                intent.putExtra("TrackToLoad", message);
                v.getContext().startActivity(intent);
			}
		});
	
	    deleteBtn.setOnClickListener(new View.OnClickListener(){
	        @Override
	        public void onClick(final View v) { 
	            Toast.makeText(v.getContext(), "Deleted", Toast.LENGTH_SHORT).show();
				AlertDialog.Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("Delete");
				alert.setMessage("Do you want to delete the track?");
				alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
		                String filename = (String) list.get(position);
		        		String root = Environment.getExternalStorageDirectory().toString();
		        		String filedirname = root + "/tracks/" + filename;
		                File file = new File(filedirname);
		                boolean deleted = file.delete();
		                if(deleted) {
		                    Toast.makeText(v.getContext(), "Deleted", Toast.LENGTH_SHORT).show();
		    	            list.remove(position); //or some other task
		    	            notifyDataSetChanged();	                    
		                } else {
		                    Toast.makeText(v.getContext(), "Error when deleting the file", Toast.LENGTH_SHORT).show();		                	
		                }
					}		
				});
				alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}		
				});				
				alert.show();
				
	            //do something

	        }
	    });
	
	    return view; 
	} 
}