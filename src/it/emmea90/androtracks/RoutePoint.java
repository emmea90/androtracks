package it.emmea90.androtracks;

import com.google.android.gms.maps.model.LatLng;

/**
 * Class to manage a route point
 * @author Alessio Massetti
 *
 */
public class RoutePoint implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4938489977752717604L;
	private float altitude;
	private float canvasx;
	private float canvasy;
	private float distance;
	private LatLng position;
}
